# -*- coding: utf-8 -*-
# <nbformat>3.0</nbformat>

# <codecell>

import maabara as ma
import numpy as np
import scipy as sp
import sympy as sym
import matplotlib as mpl 
import matplotlib.pyplot as plt
import math
import matplotlib.gridspec as gridspec
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
mpl.rcParams['text.usetex']=True
mpl.rcParams['text.latex.unicode']=True
mpl.rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})

###1,90 A:

Ua, Ia = np.loadtxt('1_9.dat',unpack=True)

#Fehler:
sUa=abs(Ua*0.25/100)+0.01  #0.25% + 1D
sIa_mikro=Ia[[0,1,2,3,4]]*0+0.0005  #Fehler des Mikroampere-Messgerätes
sIa_meter=Ia[Ia>0.05]*0.8/100+0.02  #Fehler des Amperemeters (0.8% + 2D)
sIa=np.append(sIa_mikro,sIa_meter)  #Gesamter Fehelrarray

###1,90 A:

Ua2, Ia2 = np.loadtxt('1_95.dat',unpack=True)

#Fehler:
sUa2=abs(Ua2*0.25/100)+0.01  #0.25% + 1D
sIa2_mikro=Ia2[[0,1,2,3,4,5]]*0+0.0005  #Fehler des Mikroampere-Messgerätes
sIa2_meter=Ia2[Ia2>0.05]*0.8/100+0.02  #Fehler des Amperemeters (0.8% + 2D)
sIa2=np.append(sIa2_mikro,sIa2_meter)  #Gesamter Fehelrarray

###1,90 A:

Ua3, Ia3 = np.loadtxt('2_0.dat',unpack=True)

#Fehler:
sUa3=abs(Ua3*0.25/100)+0.01  #0.25% + 1D
sIa3_mikro=Ia3[[0,1,2,3,4,5,6]]*0+0.0005  #Fehler des Mikroampere-Messgerätes
sIa3_meter=Ia3[Ia3>0.05]*0.8/100+0.02  #Fehler des Amperemeters (0.8% + 2D)
sIa3=np.append(sIa3_mikro,sIa3_meter)  #Gesamter Fehelrarray

###Plot der Kennlinien:

plt.close('all')
plt.clf
plt.figure(1)

ax1 = plt.subplot2grid((3,3), (0,0), colspan=3,rowspan=1)
ax2 = plt.subplot2grid((3,3), (1,0), colspan=3,rowspan=3)

ax1.ticklabel_format(style='sci', axis='both', scilimits=(-3,3))
ax2.ticklabel_format(style='sci', axis='both', scilimits=(-3,3))

markers=2
ax1.errorbar(Ua,Ia,sUa,sIa,'ro',label=r'$I_H=1.90$\,A',markersize=markers)
ax1.errorbar(Ua2,Ia2,sUa2,sIa2,'go',label=r'$I_H=1.95$\,A',markersize=markers)
ax1.errorbar(Ua3,Ia3,sUa3,sIa3,'bo',label=r'$I_H=2.00$\,A',markersize=markers)

markers=2
ax2.set_xlim(-5,21)
ax2.set_ylim(-0.5,4.5)
ax2.errorbar(Ua,Ia,sIa,sUa,'ro',label=r'$I_H=1.90$\,A',markersize=markers)
ax2.errorbar(Ua2,Ia2,sIa2,sUa2,'go',label=r'$I_H=1.95$\,A',markersize=markers)
ax2.errorbar(Ua3,Ia3,sIa3,sUa3,'bo',label=r'$I_H=2.00$\,A',markersize=markers)
ax2.legend(loc=4)
ax2.set_xticks(np.arange(-4,21,2))
ax2.set_yticks(np.arange(-0.5,4.5,0.5))
ax2.set_ylabel('Anodenstrom $I_A$ [mA]')
ax2.set_xlabel('Anodenspannung $U_A$ [V]')
minorLocator   = MultipleLocator(0.5)
ax2.xaxis.set_minor_locator(minorLocator)

plt.savefig("plot1.pdf",transparent=True,format="pdf",bbox_inches='tight')




# <codecell>

###Linearer Plot:

Ualin,Ialin=np.loadtxt('1_9lin.dat',unpack=True)
sUalin=abs(Ualin*0.25/100)+0.01  #0.25% + 1D
sIalin=Ialin*0.8/100+0.02  #Fehler des Amperemeters (0.8% + 2D)

linear=ma.uncertainty.Sheet('a**(2/3)','I_A^{2/3}')

t=np.c_[Ialin,sIalin]
sIalin=linear.batch(t,'a|a%')
linear.p()

Ualin2,Ialin2=np.loadtxt('1_95lin.dat',unpack=True)
sUalin2=abs(Ualin2*0.25/100)+0.01  #0.25% + 1D
sIalin2=Ialin2*0.8/100+0.02  #Fehler des Amperemeters (0.8% + 2D)

linear=ma.uncertainty.Sheet('a**(2/3)','I_A^{2/3}')

t=np.c_[Ialin2,sIalin2]
sIalin2=linear.batch(t,'a|a%')
linear.p()

Ualin3,Ialin3=np.loadtxt('2_0lin.dat',unpack=True)
sUalin3=abs(Ualin3*0.25/100)+0.01  #0.25% + 1D
sIalin3=Ialin3*0.8/100+0.02  #Fehler des Amperemeters (0.8% + 2D)

linear=ma.uncertainty.Sheet('a**(2/3)','I_A^{2/3}')

t=np.c_[Ialin3,sIalin3]
sIalin3=linear.batch(t,'a|a%')
linear.p()

                

# <codecell>

###Plot von I_A ^ (2/3) , Abgrenzung des Raumladungsbereiches:

#Fit:
m1,b1,tex1=ma.data.linear_fit(Ualin,sIalin[:,0],sIalin[:,1],'f')
m2,b2,tex2=ma.data.linear_fit(Ualin2,sIalin2[:,0],sIalin2[:,1],'g')
m3,b3,tex3=ma.data.linear_fit(Ualin3,sIalin3[:,0],sIalin3[:,1],'h')

f2, ax=plt.subplots()

a=(2./3.)




x=np.linspace(-10,max(Ualin))
ax.plot(x,m1.n*x+b1.n,'r-',label='$'+tex1+'$')
x=np.linspace(-10,max(Ualin2))
ax.plot(x,m2.n*x+b2.n,'g-',label='$'+tex2+'$')
x=np.linspace(-10,max(Ualin3))
ax.plot(x,m3.n*x+b3.n,'b-',label='$'+tex3+'$')

ax.errorbar(Ualin,sIalin[:,0],sIalin[:,1],sUalin,'ro',label=r'$I_H=1.90$\,A',markersize=markers)
ax.errorbar(Ualin2,sIalin2[:,0],sIalin2[:,1],sUalin2,'go',label=r'$I_H=1.90$\,A',markersize=markers)
ax.errorbar(Ualin3,sIalin3[:,0],sIalin3[:,1],sUalin3,'bo',label=r'$I_H=1.90$\,A',markersize=markers)

ax.set_ylabel(r'Anodenstrom zur Potenz 2/3 $I_A^{\frac{2}{3}}$ [mA]')
ax.set_xlabel('Anodenspannung $U_A$ [V]')
minorLocator   = MultipleLocator(1)
ax.xaxis.set_minor_locator(minorLocator)
ax.set_xlim(-5,18)
ax.set_ylim(-0.5,2.6)

plt.legend(loc=2)
plt.savefig("plot2.pdf",transparent=True,format="pdf",bbox_inches='tight')

# <codecell>

###Berechnung der Kontaktspannung:
kont=ma.uncertainty.Sheet('b/m','U_K')
kont.v('b',b1.n,b1.s)
kont.v('m',m1.n,m1.s)
uk1=kont.p()
kont.v('b',b2.n,b2.s)
kont.v('m',m2.n,m2.s)
uk2=kont.p()
kont.v('b',b3.n,b3.s)
kont.v('m',m3.n,m3.s)
uk3=kont.p()

# <codecell>

ma.data.weighted_average([[uk1.n,uk1.s],[uk2.n,uk2.s],[uk3.n,uk3.s]])

# <codecell>


